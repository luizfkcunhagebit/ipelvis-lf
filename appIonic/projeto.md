Atividades Luiz Filipe
-

**Componentes**
- PerguntaComponent
    - Recebe uma estrutura de pergunta pré-definida (com respostas), e oferece como "retorno" um dos objetos de resposta dentro.
    - As respostas para a pergunta poderão ser em lista, ou em range crescente ou decrescente.

**Telas**
- Menu Principal
- Questionario
    - Essa tela deve saber gerenciar a utilização do questionário component.
- Meu diário -> Calendário
- Meu diário -> Estatísticas
- Dicas
- Sobre
- Políticas de privacidade
- Pagamento