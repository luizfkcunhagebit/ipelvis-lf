import { Component, ViewChildren, ViewChild, QueryList } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { API_ENDPOINT } from '../../constants/api-constants';
import { map } from 'rxjs/operators';

declare var google;

@IonicPage()
@Component({
  selector: 'page-estatisticas',
  templateUrl: 'estatisticas.html',
})
export class EstatisticasPage {

  // Element containing charts div
  @ViewChildren('charts') chartsDiv: QueryList<any>;

  private statistics: any[];

  constructor(private _http: HttpClient) {
  }


  ionViewWillEnter() {
    // Recupera estatísticas
    this.getStatistics();
  }


  // Recupera as estatísticas para o gráfico
  getStatistics() {
    this._http
        .get(API_ENDPOINT.concat('classe=Mobile&metodo=CarregarDadosEstatisticaPorIdPaciente&atributo[idpaciente]=' + JSON.parse(window.localStorage.paciente).IdPaciente))
        .subscribe((data: any) => {
          this.statistics = data.ClassData.List;
          this.chartsDiv.changes.subscribe((r) => { 
            this.loadCharts(data.ClassData.List);
          });
        });
  }

  /**
   * Load chart component 
   * @param statistics - Estatísticas do servidor
   */
  loadCharts(statistics: any) {
    let i = 0;

    statistics.forEach((item) => {
      console.log(item)
      if (item.respostas && item.respostas.length > 0) {
        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('date', 'Data');
        data.addColumn('number', 'Resposta');

        item.respostas.forEach((resposta) => {
          data.addRow([new Date(resposta.DataDevolutiva), +resposta.Peso]);
        });

        // Set chart options
        var options = {'title': item.pergunta,
                        'width':'95vw',
                        'backgroundColor': {fill: 'transparent'},
                        'legend': {position: 'none'},
                        'hAxis': {
                          format: 'd/M'
                        }
                      };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.charts.Line(this.chartsDiv.toArray()[i].nativeElement);
        chart.draw(data, google.charts.Line.convertOptions(options));
      } 
      // Index para utilização dos elementos de gráficos
      i++;
    });
  }

}
