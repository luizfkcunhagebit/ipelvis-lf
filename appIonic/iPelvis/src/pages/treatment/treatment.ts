import { Component } from '@angular/core';
import { IonicPage, NavParams, NavController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { API_ENDPOINT } from '../../constants/api-constants';
import { tap } from 'rxjs/operators';
import { Midia } from '../../entities/midia';
import {PatientProvider} from "../../providers/patient/patient";


@IonicPage()
@Component({
  selector: 'page-treatment',
  templateUrl: 'treatment.html',
})
export class TreatmentPage {

  private treatment: any;

  private fases: number[] = [];

  private faseSelected: any = 1;

  private midiaList: Midia[] = [];

  private midiaSelected: Midia = new Object as Midia;

  private mostrarBotaoQuestionario: boolean = false;

  private paciente : any;
  /**
   *  Define o tipo da tela
   *  - Apresentação
   *  - Tratamento
   *  - Exercício
   */
  private tipo: string;

  constructor(private _http: HttpClient,
              private _navParams: NavParams,
              private _navCtrl: NavController,
              private _patientProvider : PatientProvider) {

    this.tipo = this._navParams.get('tipo');
    this.paciente = JSON.parse(window.localStorage.paciente);
    this.mostrarBotaoQuestionario = false;
  }

  /**
   * Initialize class components and data
   */
  ionViewWillEnter() {
    switch (this.tipo)
    {
      case 'apresentacao':
        this.getApresentacao();
        break;
      case 'tratamento':
        this.getTreatment();
        break;
      case 'dica':
        this.getDicas();
        break;
      case 'exercicio':
        this.getExercicios();
        break;
    }
  }

  /**
   * Retrieve treatment
   */
  private getTreatment() {
    this._http
        .get(API_ENDPOINT.concat('classe=Mobile&metodo=CarregarTratamentoPorId&atributo[idtratamento]='+this.paciente.IdTratamento))
        .pipe(
          tap(data => console.log(data))
        )
        .subscribe((data: any) => {
          this.treatment = data.ClassData.List;

          // Add treatment phases to variable
          for (let i=0; i < this.treatment.fases.length; i++)
            this.fases.push(i+1);

          if(!this.treatment.fases)
          {
            this._navCtrl.pop();
            return;
          }

          // Set midia list based on the phase selected
          this.midiaList = this.treatment.fases[this.faseSelected-1].midias;

          // Set midia selected as first
          this.midiaSelected = this.treatment.fases[this.faseSelected-1].midias[0];

          this.salvarVisualizacao(this.midiaSelected.IdMidiaFase);

          this.verificaExibicaoQuestionario();
        });
  }

  private getApresentacao() {
    this._http
        .get(API_ENDPOINT.concat('classe=Mobile&metodo=CarregarApresentacao'))
        .pipe(
          tap(data => console.log(data))
        )
        .subscribe((data: any) => {
          this.treatment = data.ClassData.List;
          for (let i=0; i < this.treatment.fases.length; i++)
            this.fases.push(i+1);

          if(!this.treatment.fases)
          {
            this._navCtrl.pop();
            return;
          }
          // Set midia list based on the phase selected
          this.midiaList = this.treatment.fases[this.faseSelected-1].midias;
          // Set midia selected as first
          this.midiaSelected = this.treatment.fases[this.faseSelected-1].midias[0];
          this.salvarVisualizacao(this.midiaSelected.IdMidiaFase);
          this.verificaExibicaoQuestionario();
        });
  }

  private getExercicios() {
    this._http
        .get(API_ENDPOINT.concat('classe=Mobile&metodo=CarregarExerciciosPorIdTratamento&atributo[idtratamento]='+this.paciente.IdTratamento))
        .pipe(
          tap(data => console.log(data))
        )
        .subscribe((data: any) => {
          this.treatment = data.ClassData.List;
          for (let i=0; i < this.treatment.fases.length; i++)
            this.fases.push(i+1);

          if(!this.treatment.fases)
          {
            this._navCtrl.pop();
            return;
          }
          // Set midia list based on the phase selected
          this.midiaList = this.treatment.fases[this.faseSelected-1].midias;
          // Set midia selected as first
          this.midiaSelected = this.treatment.fases[this.faseSelected-1].midias[0];
          this.salvarVisualizacao(this.midiaSelected.IdMidiaFase);
          //NUNCA MOSTRA BOTAO QUESTIONARIO this.verificaExibicaoQuestionario();
        });
  }

  private getDicas() {
    this._http
        .get(API_ENDPOINT.concat('classe=Mobile&metodo=CarregarTratamentoPorId&atributo[idtratamento]='+this._navParams.get('id')))
        .pipe(
          tap(data => console.log(data))
        )
        .subscribe((data: any) => {
          this.treatment = data.ClassData.List;
          for (let i=0; i < this.treatment.fases.length; i++)
            this.fases.push(i+1);

          if(!this.treatment.fases)
          {
            this._navCtrl.pop();
            return;
          }
          // Set midia list based on the phase selected
          this.midiaList = this.treatment.fases[this.faseSelected-1].midias;
          // Set midia selected as first
          this.midiaSelected = this.treatment.fases[this.faseSelected-1].midias[0];

          this.salvarVisualizacao(this.midiaSelected.IdMidiaFase);

          this.verificaExibicaoQuestionario();
        });
  }

  /**
   * Method executed when fase is changed
   */
  onClickChangeFase() {
    // Set midia list based on the phase selected
    this.midiaList = this.treatment.fases[this.faseSelected-1].midias;
  }

  /**
   * Method executed when midia is selected
   */
  onMidiaSelected(midia: Midia) {
    this.midiaSelected = midia;
    this.salvarVisualizacao(midia.IdMidiaFase);
  }

  salvarVisualizacao(idMidiaFase)
  {
    this._http.get(API_ENDPOINT+'classe=Mobile&metodo=SalvarVisualizacao&atributo[idpaciente]='+this.paciente.IdPaciente+'&atributo[idmidiafase]='+idMidiaFase).subscribe(data => {
      console.log('visualizacaoMF');
      console.log(data);
    });
  }

  onClickQuizButton() {
    let tipoQ = '';
    if(this.tipo == 'apresentacao')
      tipoQ = 'q1';
    else if(this.tipo == 'tratamento')
      tipoQ = 'q3';
    this._navCtrl.push('QuestionarioPage', {tipo: tipoQ});
  }

  verificaExibicaoQuestionario()
  {
    switch (this.tipo)
    {
      case 'apresentacao':
          if(!this.paciente.IdTratamento)
            this.mostrarBotaoQuestionario = true;
        break;
      case 'tratamento':
        this._patientProvider.obterUltimaDevolutiva(window.localStorage.codigoPaciente).subscribe(data => {
          console.log(data);
          if (data.ClassData.List && data.ClassData.List.length > 0) {
            let devolutiva = data.ClassData.List[0], now = new Date(), interval = now.getTime() - (new Date(devolutiva.DataDevolutiva).getTime());
            //verifica quanto tempo da última devolutiva e do tratamento
            if(interval > parseInt(this.treatment.IntervaloDevolutiva)*24*60*60*1000)
            {
              //TODO: quando nova fase entra
              this.mostrarBotaoQuestionario = true;
            }
          }else{
            this.mostrarBotaoQuestionario = true;
          }
        });
        break;
    }
  }

}
