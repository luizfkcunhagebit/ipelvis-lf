import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TreatmentPage } from './treatment';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    TreatmentPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(TreatmentPage),
  ],
})
export class TreatmentPageModule {}
