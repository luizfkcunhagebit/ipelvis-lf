import { Component, ViewChild } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams, Slides} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {PatientProvider} from "../../providers/patient/patient";
import { API_ENDPOINT } from '../../constants/api-constants';

@IonicPage()
@Component({
  selector: 'page-questionario',
  templateUrl: 'questionario.html',
})
export class QuestionarioPage {

  @ViewChild(Slides) slides: Slides;

  tipo : string;
  title : string;
  perguntaAtual : number = 1;
  data : Observable<any>;
  perguntas : any = [];
  parametroPerguntaRequisicao : any;
  respostas : any  = [];
  paciente : any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public http:HttpClient,
              private alertCtrl: AlertController,
              private _patientProvider: PatientProvider) {
    if(this.navParams.get('tipo'))
      this.tipo = this.navParams.get('tipo');
    if(this.navParams.get('paramId'))
      this.parametroPerguntaRequisicao = this.navParams.get('paramId');
    this.paciente = JSON.parse(window.localStorage.paciente);

    this.init();
  }

  ionViewDidLoad() {
    this.slides.lockSwipes(true)
  }

  init()
  {
    if(this.tipo == null || this.tipo == 'q1' || this.tipo == 'q2')
    {
      this.title = 'Questionário de apresentação';
    }else{
      this.title = 'Questionário de acompanhamento';
    }
    this.loadData();
  }

  loadData()
  {
    this.perguntas = [];//reinicia perguntas
    if(this.tipo == null || this.tipo == 'q1')
    {
      let alert = this.alertCtrl.create({
        title: 'Questionário de apresentação',
        subTitle: 'Gostaríamos de saber mais sobre o que incomoda você. Nos adaptamos a sua necesside, as perguntas à seguir irão determinar qual tratamento você irá realizar. Basta marcar uma opção e clicar em "Finalizar"',
        buttons: ['Fechar']
      });
      alert.present();
      console.log('alert');
      //prepara estrutura padrão a ser usada pelo componente
      //carrega respostas
      this.data = this.http.get(API_ENDPOINT + 'classe=Mobile&metodo=CarregarPerguntaInicialQ1');
      this.data.subscribe(data => {
        let respostas = [];
        for(let i = 0; i < data.ClassData.List.length; i++)
        {
          respostas.push({
            chave: data.ClassData.List[i].IdAlternativaChave,
            valor: data.ClassData.List[i].Alternativa,
            request: data.ClassData.List[i]
          });
        }
        this.perguntas[0] = {
          titulo: 'O que mais te incomoda?',
          descricao : '',
          respostas : respostas,
          tipo: 'lista',
          resposta: {}
        };
      });
    }else if(this.tipo == 'q2'){
      //prepara estrutura padrão a ser usada pelo componente
      //carrega respostas
      this.data = this.http.get(API_ENDPOINT + 'classe=Mobile&metodo=CarregarPerguntaInicialQ2PorIdAlternativaChaveQ1&atributo[idalternativachave]='+this.parametroPerguntaRequisicao);
      this.data.subscribe(data => {
        let respostas = [];
        for(let i = 0; i < data.ClassData.List.length; i++)
        {
          respostas.push({
            chave: data.ClassData.List[i].IdAlternativaTratamento,
            valor: data.ClassData.List[i].Pergunta,
            request: data.ClassData.List[i]
          });
        }
        this.perguntas[0] = {
          titulo: 'Quando você mais sente esse incomodo?',
          descricao : '',
          respostas : respostas,
          tipo: 'lista',
          resposta: {}
        };
      });
    }else{

      if(this.navParams.get('primeiraUtilizacao'))
      {
        let alert = this.alertCtrl.create({
          title: 'Questionário de acompanhamento',
          subTitle: 'Agora já sabemos para qual tratamento direcioná-la. Em seguida você responderá o questionário de acompanhamento pela primeira vez. Ao longo do tratamento você responderá outras vezes este mesmo questionário e poderá acompanhar e comparar as respostas na opção Meu Diário.',
          buttons: ['Fechar']
        });
        alert.present();
      }

      this.data = this.http.get(API_ENDPOINT + 'classe=Mobile&metodo=CarregarTratamentoPerguntaPorIdTratamento&atributo[idtratamento]='+this.paciente.IdTratamento);
      this.data.subscribe(data => {
        //console.log(data);
        for(let i = 0; i < data.ClassData.List.length; i++)
        {
          let respostas = [];
          for(let j = 0; j < data.ClassData.List[i].respostas.length; j++)
          {
            respostas.push({
              chave: data.ClassData.List[i].respostas[j].IdResposta,
              valor: data.ClassData.List[i].respostas[j].Descricao,
              request: data.ClassData.List[i].respostas[j]
            });
          }
          this.perguntas.push({
            titulo: data.ClassData.List[i].Pergunta,
            descricao : '',
            respostas : respostas,
            tipo: 'lista', //TODO: AJUSTAR
            request: data.ClassData.List[i],
            resposta: {}
          });
        }
      });
    }
  }

  respostaEscolhida(pergunta)
  {
    this.respostas[this.slides.getActiveIndex()] = (pergunta);
  }

  finalizar()
  {
    //console.log(this.respostas);
    if(this.respostas.length != this.perguntas.length)
    {
      let alert = this.alertCtrl.create({
        title: 'Ops',
        subTitle: 'Você não respondeu todas as perguntas',
        buttons: ['Fechar']
      });
      alert.present();
      return;
    }
    if(this.tipo == 'q1')
    {
      //console.log('vai abrir q2');
      //console.log(this.respostas[0]);
      this.navCtrl.push('QuestionarioPage', {tipo: 'q2', paramId: this.respostas[0].resposta.request.IdAlternativaChave});
    }else if(this.tipo == 'q2'){
      //podemos salvar o tratamento do usuário
      this.salvarTratamentoPaciente(this.respostas[0].resposta.request.IdAlternativaChave, this.respostas[0].resposta.request.IdTratamento);
    }else{
      this.cadastrarDevolutiva();
    }
  }

  salvarTratamentoPaciente(IdAlternativaChave, IdTratamento)
  {
    this._patientProvider.salvarCampoPaciente('IdAlternativaChave', IdAlternativaChave, window.localStorage.codigoPaciente).subscribe(data => {
      this._patientProvider.salvarCampoPaciente('IdTratamento', IdTratamento, window.localStorage.codigoPaciente).subscribe(data => {
        //Obtem dados do paciente
        this._patientProvider.obterDadosPaciente(window.localStorage.codigoPaciente).subscribe(data => {
          if (data.ClassData.List) {
            window.localStorage.paciente = JSON.stringify(data.ClassData.List[0]);
            this.navCtrl.push('QuestionarioPage', {tipo: 'q3', primeiraUtilizacao: true});
          }
        });
      });
    });
  }

  cadastrarDevolutiva()
  {
    let respostasJSON = [];
    //preparar dados para salvar
    for (let i = 0; i < this.respostas.length; i++)
      respostasJSON.push(parseInt(this.respostas[0].resposta.request.IdResposta));
    this._patientProvider.cadastrarDevolutiva(window.localStorage.codigoPaciente, JSON.stringify(respostasJSON)).subscribe(data => {
      if(data.ClassData.List == this.respostas.length)
      {
        //cadastrou
        let alert = this.alertCtrl.create({
          title: 'Opa! Que legal!',
          subTitle: 'Você respondeu as perguntas de acompanhamento (:',
          buttons: [{
            text: 'Fechar',
            handler: () => {
              this.navCtrl.setRoot('MenuPage');
            }
          }]
        });
        alert.present();
      }
    });
  }

  proximaPergunta()
  {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }


  perguntaAnterior()
  {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

}
