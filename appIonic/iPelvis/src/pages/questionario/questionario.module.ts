import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionarioPage } from './questionario';
import {ComponentsModule} from "../../components/components.module";

@NgModule({
  declarations: [
    QuestionarioPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionarioPage),
    ComponentsModule
  ],
})
export class QuestionarioPageModule {}
