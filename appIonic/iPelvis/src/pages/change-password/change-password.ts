import { Component } from '@angular/core';
import { IonicPage, ToastController } from 'ionic-angular';
import { PatientProvider } from '../../providers/patient/patient';

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

  private password: string;
  private repeatPassword: string;

  constructor(private _toastCtrl: ToastController,
              private _patientService: PatientProvider) {
  }


  /**
   * On button change password click
   */
  onClickChangePassword() {
    if (this.password != this.repeatPassword) {
      this.showToastMessage();
      return;
    }

    //this._patientService.salvarCampoPaciente('password', this.password, localStorage.getItem());
  }

  /**
   * Exhibit toast message
   */
  showToastMessage() {
    let toast = this._toastCtrl.create({
      message: 'As senhas digitadas são diferentes.',
      duration: 3000,
      position: 'top'
    });

    toast.present();
  }

}
