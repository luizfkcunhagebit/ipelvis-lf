import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-meudiario',
  templateUrl: 'meudiario.html',
})
export class MeudiarioPage {
  
  calendarioRoot = 'CalendarioPage';
  estatisticasRoot = 'EstatisticasPage';

  constructor(private _http: HttpClient) {
  }

}
