import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeudiarioPage } from './meudiario';

@NgModule({
  declarations: [
    MeudiarioPage,
  ],
  imports: [
    IonicPageModule.forChild(MeudiarioPage),
  ],
})
export class MeudiarioPageModule {}
