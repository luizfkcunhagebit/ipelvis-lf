import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {API_ENDPOINT} from "../../constants/api-constants";
import {PatientProvider} from "../../providers/patient/patient";

/**
 * Generated class for the AtivacaoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ativacao',
  templateUrl: 'ativacao.html',
})
export class AtivacaoPage {

  paciente : any;
  codigoAtivacao : string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http : HttpClient, private alertCtrl: AlertController, public _patientProvider : PatientProvider) {
    this.carregarPaciente();
  }

  ionViewDidLoad() {
  }

  carregarPaciente()
  {
    this.paciente = JSON.parse(window.localStorage.paciente);
    this.codigoAtivacao = this.paciente.TokenAtivacao;
  }

  onClickAtivar()
  {
    this.http.get(API_ENDPOINT+'classe=Mobile&metodo=UsaCodigoAtivacaoPaciente&atributo[codigoativacao]='+this.codigoAtivacao+'&atributo[codigo]='+window.localStorage.codigoPaciente).subscribe((data : any) => {
      if(data.ClassData.List)
      {
        let title = '', msg = '';
        switch(data.ClassData.List)
        {
          case 4:
            title = 'Ativação realizada com sucesso';
            msg = 'Muito obrigado por realizar a compra do iPelvis.';
            break;
          case 1:
            title = 'Ops';
            msg = 'Esse código já foi utilizado por outro paciente';
            break;
          case 2:
            title = 'Ops';
            msg = 'Código de ativação inválido';
            break;
          case 3:
            title = 'Ops';
            msg = 'Abra e fecha o aplicativo e tente novamente.';
            break;
        }
        let alert = this.alertCtrl.create({
          title: title,
          message: msg,
          buttons: [
            {
              text: 'Fechar',
              role: 'cancel',
              handler: () => {
                this.atualizarPaciente();
              }
            }
          ]
        });
        alert.present();
      }
  });
  }

  atualizarPaciente() {
    if(window.localStorage.codigoPaciente)
    {
      this._patientProvider.obterDadosPaciente(window.localStorage.codigoPaciente).subscribe(data => {
        if (data.ClassData.List) {
          window.localStorage.paciente = JSON.stringify(data.ClassData.List[0]);
          this.paciente = JSON.parse(window.localStorage.paciente);
        }
      });
    }
  }

}
