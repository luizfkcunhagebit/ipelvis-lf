import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AtivacaoPage } from './ativacao';

@NgModule({
  declarations: [
    AtivacaoPage,
  ],
  imports: [
    IonicPageModule.forChild(AtivacaoPage),
  ],
})
export class AtivacaoPageModule {}
