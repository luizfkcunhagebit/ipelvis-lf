import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-config',
  templateUrl: 'config.html',
})
export class ConfigPage {

  constructor(public _navCtrl: NavController, public _navParams: NavParams) {
  }

  /**
   * Redirect to change password page
   */
  onClickChangePassword() {
    this._navCtrl.push('ChangePasswordPage');
  }

  onClickCodigoAtivacao()
  {
    this._navCtrl.push('AtivacaoPage');
  }

}
