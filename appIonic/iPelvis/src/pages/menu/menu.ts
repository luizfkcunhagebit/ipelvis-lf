import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {PatientProvider} from "../../providers/patient/patient";

/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  pacienteTratamento: boolean = false;
  pacientePagou: boolean = false;
  paciente: any = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private _patientProvider: PatientProvider,
              private alertCtrl: AlertController) {
    //verifica se existe codigo do paciente no storage
    this.carregarPaciente();
  }

  ionViewDidLoad()
  {

  }

  verificaPagamento()
  {
    let alert = this.alertCtrl.create({
      title: 'Comprar iPelvis',
      message: 'Essa área é exclusiva para clientes pagantes. Se você possui um código de ativação clique em inserir código. Clique em comprar para ser redirecionado ao pagamento',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Inserir código ativação',
          handler: () => {
            this.navCtrl.push('AtivacaoPage');
          }
        },
        {
          text: 'Comprar',
          handler: () => {
            let alert = this.alertCtrl.create({
              title: 'Em breve',
              subTitle: 'Atualmente você pode desbloquear o aplicativo somente inserindo um código. Para obter um código entre em contrato com nossa equipe através do e-mail contato@ipelvis.com',
              buttons: ['Fechar']
            });
            alert.present();
          }
        }

      ]
    });
    if(!this.pacientePagou)
    {
      alert.present();
    }

    return this.pacientePagou;
  }

  openTratamento() {
    if(!this.verificaPagamento())
      return;
    if (this.pacienteTratamento)
      this.navCtrl.push('TreatmentPage', {tipo: 'tratamento'});
  }

  openApresentacao() {
    this.navCtrl.push('TreatmentPage', {tipo: 'apresentacao'});
  }

  openMeuDiario() {
    if(!this.verificaPagamento())
      return;
    if (this.pacienteTratamento)
      this.navCtrl.push('MeudiarioPage');
  }

  openExercicio() {
    if(!this.verificaPagamento())
      return;

    let alert = this.alertCtrl.create({
      title: 'Em breve',
      message: 'Em breve teremos uma versão do aplicativo com games para a realização dos exercícios.',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
    return;
    /*if (this.pacienteTratamento)
      this.navCtrl.push('TreatmentPage', {tipo: 'exercicio'});*/
  }

  openDicas() {
    if(!this.verificaPagamento())
      return;
    if (this.pacienteTratamento)
      this.navCtrl.push('DicasPage');
  }

  openConfig() {
    if (this.pacienteTratamento)
      this.navCtrl.push('ConfigPage');
  }

  openQuestionario() {
    this.navCtrl.push('QuestionarioPage', {tipo: "apresentacao"});
  }

  carregarPaciente() {
    if(window.localStorage.codigoPaciente)
    {
      this._patientProvider.obterDadosPaciente(window.localStorage.codigoPaciente).subscribe(data => {
        if (data.ClassData.List) {
          window.localStorage.paciente = JSON.stringify(data.ClassData.List[0]);
          this.paciente = JSON.parse(window.localStorage.paciente);
          if(this.paciente.IdTratamento)
          {
            this.pacienteTratamento = true;
          }
          if(this.paciente.Pagante == 1)
          {
            this.pacientePagou = true;
          }
        }
      });
    }
  }

}
