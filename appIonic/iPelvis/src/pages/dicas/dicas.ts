import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {API_ENDPOINT} from "../../constants/api-constants";

/**
 * Generated class for the DicasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dicas',
  templateUrl: 'dicas.html',
})
export class DicasPage {

  paciente: any;
  dicas: any;
  backgrounds: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _http: HttpClient) {
  }

  ionViewDidLoad() {
    this.paciente = JSON.parse(window.localStorage.paciente);
  }

  ionViewWillEnter()
  {
    this.carregarDicas();
  }

  carregarDicas()
  {
    this._http.get(API_ENDPOINT+'classe=Mobile&metodo=CarregarDicasPorIdTratamento&atributo[idtratamento]='+this.paciente.IdTratamento).subscribe((data : any) => {
      if(data.ClassData.List)
      {
        this.dicas = data.ClassData.List;
        //melhorar
        this.backgrounds = ['#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d', '#ffaa38', '#ffaa38', '#fcd73d', '#fcd73d'];
      }
    });
  }

  onDicaClick(dica)
  {
    this.navCtrl.push('TreatmentPage', {tipo: 'dica', id:dica.IdTratamentoDicaFilha});
  }

}
