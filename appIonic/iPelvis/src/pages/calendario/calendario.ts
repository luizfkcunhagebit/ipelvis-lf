import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { API_ENDPOINT } from '../../constants/api-constants';
import { map } from 'rxjs/operators';

@IonicPage()
@Component({
  selector: 'page-calendario',
  templateUrl: 'calendario.html',
})
export class CalendarioPage {

  datasource = [];
  calendar = {
    mode: 'month',
    currentDate: new Date(),
    noEventsLabel: 'Sem eventos',
    allDayLabel: 'Atividades realizadas'
  };


  constructor(private _http: HttpClient) {
  }

  ionViewWillEnter() {
    this.loadEventos();
  }


  /**
   * Carrega eventos do calendário
   */
  loadEventos() {
    this._http
        .get(API_ENDPOINT.concat('classe=Mobile&metodo=CarregarDadosCalendarioTodosPorIdPaciente&atributo[idpaciente]=' + JSON.parse(window.localStorage.paciente).IdPaciente))
        .pipe(
          map((data: any) => {
            console.log(data);

            data.ClassData.List.forEach(event => {
              event.title = event.quantidade;
              event.startTime = new Date(event.data);
              event.endTime = new Date(event.data);
              event.allDay = true;
            });

            return data.ClassData.List;
          })
        )
        .subscribe((data: any) => {
          console.log(data);
          this.datasource = data;
        });
  }

  onCurrentDateChanged(e)
  {

  }
  reloadSource(start, end)
  {

  }
  onEventSelected(e){

  }
  onViewTitleChanged(e)
  {

  }
  onTimeSelected(e)
  {

  }

}
