import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import { API_ENDPOINT } from '../../constants/api-constants';

@Injectable()
export class PatientProvider {

  constructor(public http: HttpClient) {
  }

  public salvarCampoPaciente(nomeCampo: string, valorCampo: string, pacienteCodigo: string): Observable<any> {
    return this.http.get(API_ENDPOINT + 'classe=Mobile&metodo=SalvarCampoDeUmPaciente&atributo[campo]='+nomeCampo+'&atributo[campovalue]='+valorCampo+'&atributo[codigo]='+pacienteCodigo);
  }

  public obterDadosPaciente(pacienteCodigo: string): Observable<any>
  {
    return this.http.get(API_ENDPOINT + 'classe=Mobile&metodo=ObterPaciente&atributo[codigo]='+pacienteCodigo);
  }

  public obterDadosPacienteComCadastro(iniciais : string, email : string): Observable<any>
  {
    return this.http.get(API_ENDPOINT + 'classe=Mobile&metodo=ObterPaciente&atributo[codigo]=&atributo[iniciais]='+iniciais+'&atributo[email]'+email);
  }

  public cadastrarDevolutiva(pacienteCodigo: string, respostasJSON: string): Observable<any>
  {
    return this.http.get(API_ENDPOINT + 'classe=Mobile&metodo=CadastrarDevolutiva&atributo[codigo]='+pacienteCodigo+'&atributo[respostas]='+respostasJSON);
  }

  public obterUltimaDevolutiva(pacienteCodigo: string): Observable<any>
  {
    return this.http.get(API_ENDPOINT + 'classe=Mobile&metodo=ObterUltimaDevolutiva&atributo[codigo]='+pacienteCodigo);
  }

}
