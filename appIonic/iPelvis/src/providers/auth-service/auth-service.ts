import { Injectable } from '@angular/core';

@Injectable()
export class AuthServiceProvider {

  constructor() {
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  /**
   * Verify if user is authenticated
   * @returns boolean - True if token exists <p> False is doens't exists token
   */
  public isAuthenticated(): boolean {
    // Get token from local storage
    const token = this.getToken();

    // Verify if token exists
    if (token)
      return true;
    else
      return false;
  }

}
