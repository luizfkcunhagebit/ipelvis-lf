import { NgModule } from '@angular/core';
import { VideoPlayerComponent } from './video-player/video-player';
import { VideosListComponent } from './videos-list/videos-list';
import { IonicModule } from 'ionic-angular';
import { CommonModule } from '@angular/common';
import { PerguntaComponent } from './pergunta/pergunta';

@NgModule({
	declarations: [
		VideoPlayerComponent,
		VideosListComponent,
    	PerguntaComponent
	],
	imports: [
		CommonModule,
		IonicModule
	],
	exports: [
		VideoPlayerComponent,
		VideosListComponent,
    PerguntaComponent
	]
})
export class ComponentsModule {}
