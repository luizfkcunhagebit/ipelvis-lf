import { Component, Input, SecurityContext } from '@angular/core';
import { Midia } from '../../entities/midia';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'video-player',
  templateUrl: 'video-player.html'
})
export class VideoPlayerComponent {

  @Input()
  midia: Midia;

  constructor(private _sanitizer: DomSanitizer) {
  }

  /**
   * Url sanitized
   */
  sanitizedUrl() {
    return this._sanitizer.bypassSecurityTrustResourceUrl("https://www.youtube.com/embed/" + this.midia.Url.substring(this.midia.Url.lastIndexOf("v=")+2, this.midia.Url.length));
  }
}
