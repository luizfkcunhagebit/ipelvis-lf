import { Component, EventEmitter, Input, Output, SecurityContext } from '@angular/core';
import { Midia } from '../../entities/midia';

@Component({
  selector: 'videos-list',
  templateUrl: 'videos-list.html'
})
export class VideosListComponent {

  @Input()
  midiaList: Midia[]; 

  // Callback event
  @Output()
  onItemClicked: EventEmitter<any> =  new EventEmitter();

  // Index of selected item
  itemSelected: number = 0;

  constructor() {
  }


  onItemSelected(midia: Midia, index: number) {
    // Emit event to parent
    this.onItemClicked.emit(midia);
    // Set index selected
    this.itemSelected = index;
  }

}
