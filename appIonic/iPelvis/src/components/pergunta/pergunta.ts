import {Component, EventEmitter, Input, Output} from '@angular/core';

/**
 * Generated class for the PerguntaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

@Component({
  selector: 'pergunta',
  templateUrl: 'pergunta.html'
})
export class PerguntaComponent {
  @Input() pergunta:any;
  @Output() perguntaChange = new EventEmitter();

  resposta:any = {};
  list:any = [];

  constructor() {
  }

  onClickMethod(ans)
  {
    //console.log(ans);
    this.pergunta.resposta = ans;
    //console.log(this.pergunta);
    this.perguntaChange.emit(this.pergunta);
  }
}
