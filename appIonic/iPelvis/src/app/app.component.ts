
import {Component, OnInit} from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {PatientProvider} from "../providers/patient/patient";

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  rootPage:any = 'MenuPage';


  mainMenuPages = [
    //{ title: 'Apresentação', name: 'TreatmentPage', index: 0},
    //{ title: 'Meu tratamento', name: 'TreatmentPage', index: 1},
    //{ title: 'Meu diário', name: 'MeudiarioPage', index: 2},
    //{ title: 'Exercícios', name: 'PrivacyPolicePage', index: 3},
    //{ title: 'Dicas', name: 'PrivacyPolicePage', index: 4}
  ];

  menuPages = [
    //{ title: 'Termos de uso', name: 'TermsOfUsePage', index: 0},
    //{ title: 'Política de privacidade', name: 'PoliticaPrivacidadePage', index: 1},
    { title: 'Configuração', name: 'ConfigPage', index: 2},
    //{ title: 'Sobre', name: 'AboutPage', index: 3},
    //{ title: 'Sair', name: 'Exit', index: 4}
  ];

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private _patientProvider : PatientProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  ngOnInit(): void {
    this.verificaContaUsuario();
  }

  verificaContaUsuario() : void
  {
    if(window.localStorage.codigoPaciente)
    {
      this._patientProvider.obterDadosPaciente(window.localStorage.codigoPaciente).subscribe(data => {
        if (data.ClassData.List) {
          window.localStorage.paciente = JSON.stringify(data.ClassData.List[0]);
        }else{
          this.rootPage = 'TutorialPage';
        }
      });
    }else{
      //código do paciente não existe, redireciona para tutorial
      this._patientProvider.obterDadosPacienteComCadastro('NEW', 'novo@usuario.com').subscribe(data => {
        if (data.ClassData.List) {
          window.localStorage.paciente = JSON.stringify(data.ClassData.List[0]);
          window.localStorage.codigoPaciente = data.ClassData.List[0].Codigo;
        }
        this.rootPage = 'TutorialPage';
      });
    }
  }

  /**
   * Redirect to page
   * @param page - Object with page menu page selected
   */
  openPage(page: any) {

    if (page.name == 'Exit') {
      this.logout();
      return;
    }

    // Redirect to corresponding page
    this.rootPage = page.name;
  }

  /**
   * Método executado ao clicar no link para o menu principal
   */
  onClickMenuPrincipal() {
    this.rootPage = 'MenuPage';
  }

  /**
   * Logout from app
   */
  logout() {
    // Remove patient code from local storage
    window.localStorage.removeItem('codigoPaciente');

    this.rootPage = 'TutorialPage';
  }

}

