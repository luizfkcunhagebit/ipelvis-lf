import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';


@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

  constructor(private _authService: AuthServiceProvider) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Get original url
    let urlRequest = request.url;
        
    // If user is authenticated add token to request
    if (this._authService.isAuthenticated()) {

        // Add field with token authentication
        // urlRequest.concat('&atributo[codigo]=' + this._authService.getToken());
        urlRequest.concat('&atributo[codigo]=' + '98cfb96d4740cb6f4c874b93d718d92d');
    }
    
    // Clone original request and add new one with attribute 'codigo'
    request = request.clone({
      url: urlRequest
    });

    return next.handle(request);
  }
}