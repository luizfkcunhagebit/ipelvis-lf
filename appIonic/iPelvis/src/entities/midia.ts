export interface Midia {
    IdMidiaFase: number,
    Ordem: number,
    IdVideo: number,
    IdImagem: number,
    Descricao: string,
    Titulo: string,
    Url: string,
    UrlThumbnail: string,
    Tipo: string
}